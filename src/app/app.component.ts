import { Component, OnInit } from '@angular/core';
import { Cart, CartProduct } from './Models/cart';
import { Product } from './Models/product';
import { CartService } from './Services/cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  cart: Cart = { products: [] };

  cartItems: Array<CartProduct> = [];

  constructor(private cartService: CartService) {
  }

  ngOnInit() {
    this.cartItems = JSON.parse(localStorage.getItem('products') || "[]");
    this.cartItems.forEach( (item) => {
      this.cartService.addToCart({
        image: item.image,
        name: item.name,
        price: item.price,
        quantity: 1,
        id: item.id
      })
    })
    this.cartService.cart.subscribe((cartItems) => {
      this.cart = cartItems;
    })
  }


  title = 'store-app';
}
