import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ApiService } from 'src/app/Services/api.service';
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy {
  @Output() category = new EventEmitter<string>();

  categories : Array<string> = [];
  categoriesSubscription: Subscription | undefined;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.getCategories().subscribe( (c) => {
      this.categories = c;
    })
  }

  changeCategory(category: string): void{
    this.category.emit(category);
  }

  ngOnDestroy(): void {
    if(this.categoriesSubscription){
      this.categoriesSubscription.unsubscribe();
    }
  }

}
