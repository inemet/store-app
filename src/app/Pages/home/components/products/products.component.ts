import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/Models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  @Input() product: Product | undefined;

  @Output() emitToCart = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  addToCart(): void {
    this.emitToCart.emit(this.product);
  }

}
