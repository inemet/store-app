import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-products-header',
  templateUrl: './products-header.component.html',
  styleUrls: ['./products-header.component.css']
})
export class ProductsHeaderComponent implements OnInit {
  @Output() columnsCount = new EventEmitter<number>();
  @Output() sortingType = new EventEmitter<string>();
  @Output() productCount = new EventEmitter<number>();

  sorting: string = 'desc'
  noOfProducts: number = 10;
  constructor() { }

  ngOnInit(): void {
  }

  updateSort(sortingMethod: string): void {
    this.sorting = sortingMethod;
    this.sortingType.emit(sortingMethod);
  }

  updateNoOfProducts(numberOfProducts: number): void {
    this.noOfProducts = numberOfProducts;
    this.productCount.emit(numberOfProducts);
  }

  updateColumns(newColumnNumber: number): void{
    this.columnsCount.emit(newColumnNumber);
  }

}
