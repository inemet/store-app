import { Component, OnDestroy, OnInit } from '@angular/core';
import { Product } from 'src/app/Models/product';
import { ApiService } from 'src/app/Services/api.service';
import { CartService } from 'src/app/Services/cart.service';
import { Subscription } from 'rxjs';

const ROW_HEIGHT: { [id: number]: number } = { 1: 400, 3: 355, 4: 350 };

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  noOfColumns: number = 4;
  rowHeight = ROW_HEIGHT[this.noOfColumns];
  category: string | undefined;
  products: Array<Product> = [];
  apiSupscription: Subscription | undefined;
  sort = 'desc';
  count = '10';
  priceSorting: string | undefined;

  constructor(private cartService: CartService, private api: ApiService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(): void {
    this.api.getProducts(this.count, this.sort, this.category).subscribe((res) => {
      this.products = res;
    })
  }

  columnsCount(columnsNumber: number): void {
    this.noOfColumns = columnsNumber;
    this.rowHeight = ROW_HEIGHT[this.noOfColumns];
  }

  sortingUpdate(sorting: string): void {
    this.sort = sorting;
    if(this.sort === 'price-asc'){
      this.products.sort((a, b) => a.price - b.price);
    }

    if(this.sort === 'price-desc'){
      this.products.sort((a, b) => b.price - a.price);
    }
  }

  countUpdate(countNumber: number): void {
    this.count = countNumber.toString();
    this.getProducts();
  }

  changeCategory(updatedCategory: string): void {
    this.category = updatedCategory;
    this.getProducts();
  }

  addToCart(product: Product): void {
    this.cartService.addToCart({
      image: product.image,
      name: product.title,
      price: product.price,
      quantity: 1,
      id: product.id
    })
  }

  ngOnDestroy(): void {
    if (this.apiSupscription) {
      this.apiSupscription.unsubscribe();
    }
  }

}
