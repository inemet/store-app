import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { loadStripe } from '@stripe/stripe-js';
import { Cart, CartProduct } from 'src/app/Models/cart';
import { CartService } from 'src/app/Services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart: Cart = {
    products: [{
      image: 'https://via.placeholder.com/150',
      name: 'sneakers',
      price: 10,
      quantity: 2,
      id: 1,
    },
    {
      image: 'https://via.placeholder.com/150',
      name: 'computer',
      price: 10000,
      quantity: 1,
      id: 2,
    }]
  };

  dataSource: Array<CartProduct> = [];
  tableColumns: Array<string> = [
    'image',
    'name',
    'price',
    'quantity',
    'total',
    'actions'
  ]

  constructor(private cartService: CartService, private http: HttpClient) { }

  ngOnInit(): void {
    this.cartService.cart.subscribe( (res: Cart) => {
      this.cart = res;
      this.dataSource = this.cart.products;
    })
  }

  getTotal(products: Array<CartProduct>): number{
    return this.cartService.getTotal(products);
  }

  clearCart(): void{
    this.cartService.clearShoppingCart();
  }

  removeFromCart(element: CartProduct): void{
    this.cartService.removeFromCart(element);
  }

  addQuantity(element: CartProduct): void{
    this.cartService.addToCart(element);
  }

  removeQuantity(element: CartProduct): void{
    this.cartService.removeQuantity(element);
  }

  onCheckout(): void{
    this.http.post('http://localhost:4242/checkout', {
      products: this.cart.products
    }).subscribe(async(result: any) => {
      let stripe = await loadStripe('pk_test_51MRxeRFw0ZCrr8de2YwDADtoOb0remCW5UlK4ZIRv6n7GusKgigDNP6wXFxLsdDBK22aYlJ4upef2FXe3Rln25t300NXIHEajH');
      stripe?.redirectToCheckout({
        sessionId: result.id
      })
    })
  }
}
