import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Cart, CartProduct } from '../Models/cart';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart = new BehaviorSubject<Cart>({products: []});

  itemsFromLocalStorage: Array<CartProduct> = [];

  constructor() { }


  addToCart(product: CartProduct): void{
    const products = [...this.cart.value.products];

    const productInCart = products.find((res) => res.id === product.id)

    if(productInCart){
      productInCart.quantity += 1;
    } else {
      products.push(product);
    }

    this.cart.next({ products});

    localStorage.setItem('products',JSON.stringify(products));
  }

  getTotal(items: Array<CartProduct>): number{
    return items.map( (item) => item.price * item.quantity).reduce( (previous, current) => previous + current, 0);
  }

  clearShoppingCart(): void{
    this.cart.next( {products: []});
    localStorage.clear();
  }

  removeFromCart(item: CartProduct, updateCart = true): CartProduct[]{
    this.itemsFromLocalStorage = JSON.parse(localStorage.getItem('products') || "[]");
    const filterItemsFromLocalStorage = this.itemsFromLocalStorage.filter( (element) => element.id !== item.id);
    localStorage.setItem('products',JSON.stringify(filterItemsFromLocalStorage));
    const filteredItems = this.cart.value.products.filter( (element) => element.id !== item.id);

    if(updateCart){
      this.cart.next( {products: filteredItems});
    }

    return filteredItems;
  }

  removeQuantity(item: CartProduct): void{
    let itemRemove: CartProduct | undefined;
    let items = this.cart.value.products.map( (result) =>{
      if (result.id === item.id){
        result.quantity--;

        if(result.quantity === 0){
          itemRemove = result;
        }
      }
      return result;
    });

    if (itemRemove) {
      items = this.removeFromCart(itemRemove);
    }
  }
}
