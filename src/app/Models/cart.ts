export interface CartProduct {
  image: string;
  name: string;
  price: number;
  quantity: number;
  id: number;
}

export interface Cart {
  products: Array<CartProduct>;
}
