import { Component, Input, OnInit } from '@angular/core';
import { Cart, CartProduct } from 'src/app/Models/cart';
import { CartService } from 'src/app/Services/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private shoppingCart: Cart = { products: []};

  shoppingCart1: Cart = { products: []};
  productsQuanitity = 0;

  @Input()
  get cart(): Cart{
    return this.shoppingCart;
  }

  set cart(cart: Cart){
    this.shoppingCart = cart;

    this.productsQuanitity = cart.products.map( (res) => res.quantity).reduce((previous, current) => previous + current, 0);
  }

  constructor(private cartService: CartService) { }

  ngOnInit(): void {

  }

  getTotal(products: Array<CartProduct>): number{
    return this.cartService.getTotal(products);
  }

  clearShoppingCart(): void{
    this.cartService.clearShoppingCart();
  }

}
